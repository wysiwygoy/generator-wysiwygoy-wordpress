# generator-wysiwygoy-wordpress [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Yeoman generator for WordPress projects by Wysiwyg Oy

## Installation

First, install [Yeoman](http://yeoman.io) and generator-wysiwygoy-wordpress using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-wysiwygoy-wordpress
```

Then generate your new project:

```bash
yo wysiwygoy-wordpress
```

## License

Apache-2.0 © [Wysiwyg Oy]()

[npm-image]: https://badge.fury.io/js/generator-wysiwygoy-wordpress.svg
[npm-url]: https://npmjs.org/package/generator-wysiwygoy-wordpress
[travis-image]: https://travis-ci.org/wysiwygoy/generator-wysiwygoy-wordpress.svg?branch=master
[travis-url]: https://travis-ci.org/wysiwygoy/generator-wysiwygoy-wordpress
[daviddm-image]: https://david-dm.org/wysiwygoy/generator-wysiwygoy-wordpress.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/wysiwygoy/generator-wysiwygoy-wordpress
[coveralls-image]: https://coveralls.io/repos/wysiwygoy/generator-wysiwygoy-wordpress/badge.svg
[coveralls-url]: https://coveralls.io/r/wysiwygoy/generator-wysiwygoy-wordpress
