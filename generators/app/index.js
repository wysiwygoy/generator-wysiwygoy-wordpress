'use strict';

// Node dependencies
const yeoman = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const passwordGenerator = require('generate-password');
const dotenv = require('dotenv');

// Configurable file locations

/** @type {String} Public WWW directory (within site root directory). */
const wwwDir = 'htdocs';
/** @type {String} WordPress core directory (within public WWW directory). */
const wpCoreDir = 'wp';
/** @type {String} WordPress wp-content directory (within public WWW directory). */
const wpContentDir = 'wp-content';
/** @type {String} Absolute path to where site is mapped on Docker containers. */
const containerPath = '/data';

// Other configurable parameters
const passwordConfiguration = {
  length: 20,
  number: true,
  symbols: false,
  uppercase: true
};

module.exports = yeoman.Base.extend({
  initializing: function() {
    this.log('Initializing ' + this.appname);

    dotenv.config({
      silent: true
    });

    this.props = {
      slug: this.appname,
      containerPath: containerPath,
      hostName: this.appname + '.dev.wysiwyg.fi',
      projectDir: process.env.PROJECT_DIR || '.',
      wwwDir: wwwDir,
      wwwPath: containerPath + '/' + wwwDir,
      wpCoreDir: wpCoreDir,
      wpCorePath: wwwDir + '/' + wpCoreDir,
      wpContentDir: wpContentDir,
      wpContentPath: wwwDir + '/' + wpContentDir,
      databaseRootPassword: process.env.DB_ROOT_PASSWORD || passwordGenerator.generate(passwordConfiguration),
      databaseName: process.env.DB_NAME || this.appname + '_wp_dev',
      databaseUser: process.env.DB_USER || this.appname,
      databasePassword: process.env.DB_PASSWORD || passwordGenerator.generate(passwordConfiguration),
      databaseHost: process.env.DB_HOST || 'db',
      authKey: process.env.AUTH_KEY || passwordGenerator.generate(passwordConfiguration),
      secureAuthKey: process.env.SECURE_AUTH_KEY || passwordGenerator.generate(passwordConfiguration),
      loggedInKey: process.env.LOGGED_IN_KEY || passwordGenerator.generate(passwordConfiguration),
      nonceKey: process.env.NONCE_KEY || passwordGenerator.generate(passwordConfiguration),
      authSalt: process.env.AUTH_SALT || passwordGenerator.generate(passwordConfiguration),
      secureAuthSalt: process.env.SECURE_AUTH_SALT || passwordGenerator.generate(passwordConfiguration),
      loggedInSalt: process.env.LOGGED_IN_SALT || passwordGenerator.generate(passwordConfiguration),
      nonceSalt: process.env.NONCE_SALT || passwordGenerator.generate(passwordConfiguration)
    };

    this.composeWith(require.resolve('../bedrock'), { options: this.props });
  },

  prompting: function () {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Creating a new ' + chalk.red('WordPress project') + ' "' + chalk.yellow(this.appname) + '"'
    ));
  },

  writing: function () {
    this.template('dotenv', '.env', this.props);
    this.template('dotgitignore', '.gitignore', this.props);
    this.template('docker-compose.yml', this.props);
    this.template('nginx/site.conf', this.props);
    // this.template('web/phpinfo.php', wwwDir + '/phpinfo.php', this.props);
  },
});
