'use strict';

// Node dependencies
const yeoman = require('yeoman-generator');

module.exports = yeoman.Base.extend({
  writing: function () {
    this.log('slug=' + this.options.slug);

    this.template('composer.json', this.options);
    this.template('config/application.php', this.options);
    this.directory('config/environments');
    this.copy('LICENSE.md');
    this.copy('README.md');
    this.directory('vendor');
    this.directory('web/app', this.options.wwwDir + '/' + this.options.wpContentDir);
    this.template('web/index.php', this.options.wwwDir + '/index.php', this.options);
    this.copy('web/wp-config.php', this.options.wwwDir + '/wp-config.php');
    this.template('wp-cli.yml', this.options);
  },

  install: function () {
//    this.spawnCommand('docker', ['run', '--rm', '-v', process.cwd() + ':/app', 'composer/composer:alpine', 'install']);
  }
});
